package br.com.leadcollector.LeadCollector.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.beans.XMLEncoder;

@Entity //Anotation que diz ao hibernate que essa classe será criada no banco de dados.
public class Produto {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    //Não pode ser nulo.
    @NotNull(message = "Campos nulos")
    //Não pode ser branco ""
    @NotBlank(message = "Campo não pode estar vazio")
    //Não pode ter menos que 3 carcateres.
    @Size(message = "O nome precisa ter no mínimo 3 caracteres.")
    private String nome;
    @NotNull(message = "O Campo está nulo")
    @NotBlank(message = "Campo não pode estar vazio")
    private String descricao;
    @NotNull(message = "O Campo está nulo")
    private Double preco;

    //Criar um construtor vazio.
    public Produto() {};


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
