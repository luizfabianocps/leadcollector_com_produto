package br.com.leadcollector.LeadCollector.services;

import br.com.leadcollector.LeadCollector.models.Produto;
import br.com.leadcollector.LeadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Optional;


@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    //Método que faz a comunicação dos dados do banco com a service, para salvar o produto e devolver para a controller.
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto salvarProduto(Produto produto){

        Produto objetoProduto = produtoRepository.save(produto);
        return objetoProduto;
    }

    //Método que busca do banco e devolve para a controller todos os produtos.
    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoRepository.findAll();
    }

    //Método que faz a pesquisa no banco por ID e devolve a controller.

    public Produto buscaPorId(int id){
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        if (produtoOptional.isPresent()){
            Produto produto = produtoOptional.get();
            return produto;
        } else {
            throw new RuntimeException("O produto não foi encontrado");
        }

    }

    //Método para fazer uma atualização do produto, gravando no banco e podendo ser retornado para a controller.
    public Produto atualizarProduto(int id, Produto produto){
        Produto produtoDB = buscaPorId(id); //Recebe o ID buscado pelo método.
        produto.setId(produtoDB.getId()); //Pega o ID recebido e seta no produto.
        return produtoRepository.save(produto); //Salva o novo produto atualizado, com o mesmo ID.
    }

    //Remove o produto.
    public void removeProduto (int id) {
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
            System.out.println("Produto deletado com sucesso!!!");
        } else {
            throw new RuntimeException("Registro não existe!!!");
        }
    }



}
