package br.com.leadcollector.LeadCollector.services;

import br.com.leadcollector.LeadCollector.models.Lead;
import br.com.leadcollector.LeadCollector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    public Lead salvarLead (Lead lead) {

        Lead objetoLead = leadRepository.save(lead);

        return objetoLead;

    }

    //Retorna uma lista de Leads.
    public Iterable<Lead> lerTodosOsLeads() {
        return  leadRepository.findAll();
    }
    //Método que trás a buscar por ID com Optional.
    public Lead buscarLeadPeloId (int id) {
        //Optional pode conter alguma informação ou não.
        Optional<Lead> leadOptional = leadRepository.findById(id);
        if(leadOptional.isPresent()){
            Lead lead = leadOptional.get();
            return lead;
        } else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

    public Lead atualizarLead (int id, Lead lead){
        Lead leaddb = buscarLeadPeloId(id);
        lead.setId(leaddb.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead (int id){
        if(leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
            System.out.println("Registro deletado");
        }else {
            throw new RuntimeException("Registro não existe");
        }

    }



}
