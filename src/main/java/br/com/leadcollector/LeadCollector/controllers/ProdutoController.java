package br.com.leadcollector.LeadCollector.controllers;

import br.com.leadcollector.LeadCollector.models.Produto;
import br.com.leadcollector.LeadCollector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired // Injeção de dependencias.
    private ProdutoService produtoService;


    //Método para cadastrar produtos que envia informações da api para a service.
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto){

        Produto objetoProduto = produtoService.salvarProduto(produto);
        return objetoProduto;
    }

    //Criar método de pesquisa para todos os produtos, o spring usa uma lista do tipo Iterable.
    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoService.lerTodosOsProdutos();
    }

    //Método de pesquisa por ID, faz o envio de informações da api para a service.
    @GetMapping("/{id}")
    public Produto pesquisaPorId(@PathVariable(name = "id") int id) {
        try{
            Produto prodDB = produtoService.buscaPorId(id);
            return  prodDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //Método que atualiza as informações trazendo da api e enviando para a service.
    @PutMapping("/{id}")
    public Produto atualizarPorId(@PathVariable(name = "id") int id, @RequestBody @Valid Produto produto){
        try{
            Produto produtoDB = produtoService.atualizarProduto(id, produto);
            return produtoDB;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //Método que deleta as informações, vindas da api e passadas para a service.

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id){
        try{
            produtoService.removeProduto(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }





    public ProdutoService getProdutoService() {
        return produtoService;
    }

    public void setProdutoService(ProdutoService produtoService) {
        this.produtoService = produtoService;
    }
}
