package br.com.leadcollector.LeadCollector.controllers;

import br.com.leadcollector.LeadCollector.models.Lead;
import br.com.leadcollector.LeadCollector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController //Declaração de que esta classe é a controller.
@RequestMapping("/leads")
public class LeadController {

    @Autowired //Injeção de dependencias do spring.
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    /*De onde vem as informações de produto? (Body! Por isso precisa da anotação @RequestBody,
      e para validar os campos do produto precisa do @valid
    */
    public Lead cadastrarLead (@RequestBody @Valid Lead lead) {

        Lead objetoLead = leadService.salvarLead(lead);
        return objetoLead;
    }

    @GetMapping
    public Iterable<Lead> lerTodosOsLeads(){
        return leadService.lerTodosOsLeads();
    }

    @GetMapping("/{id}")
    public Lead pesquisarPorId(@PathVariable(name = "id") int id){ //PathVariable esqueci...!
        try{
            Lead lead  = leadService.buscarLeadPeloId(id);
            return lead;
        }catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@RequestBody @Valid Lead lead, @PathVariable(name = "id") int id){
        try {
            Lead leadDB = leadService.atualizarLead(id, lead);
            return leadDB;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead(@PathVariable(name = "id") int id){
        try {
            leadService.deletarLead(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    public LeadService getLeadService() {
        return leadService;
    }

    public void setLeadService(LeadService leadService) {
        this.leadService = leadService;
    }

}
