package br.com.leadcollector.LeadCollector.repositories;

import br.com.leadcollector.LeadCollector.models.Lead;
import org.springframework.data.repository.CrudRepository;

// o Integer é a tipagem da chave primaria da tabela Lead.
public interface LeadRepository  extends CrudRepository<Lead, Integer> {



}
