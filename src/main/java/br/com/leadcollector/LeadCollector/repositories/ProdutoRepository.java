package br.com.leadcollector.LeadCollector.repositories;

import br.com.leadcollector.LeadCollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {


}
